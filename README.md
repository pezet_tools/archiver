# archiver



## Getting started

This tool doesn't need any external libraries. To run it simply open a console and type
```shell
python archiver -s <source_path> -t <target_path>
```

| Parameter           | Description                              |
|---------------------|------------------------------------------|
| -s, --source_path   | Path to a file or directory to copy      |
| -t, --target_path   | Path to a file or directory to copy into |
