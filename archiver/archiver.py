import os
import logging
import argparse
import time
from pathlib import Path


logger = logging.getLogger("ArchiverLogger")
FORMAT = '[%(asctime)s]: %(name)s %(levelname)s - %(message)s'
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
logging.basicConfig(format=FORMAT, datefmt=DATE_FORMAT)
logger.setLevel(logging.INFO)


class PathElement:

    def __init__(self, source_path: Path, source_dir: Path, target_dir: Path):
        self.source_path = source_path
        self._source_dir = source_dir
        self._target_dir = target_dir
        self.target_path = self.build_trg_path()

    def build_trg_path(self) -> Path:
        if self.source_path == self._source_dir:
            _source_dir = self.source_path.parent
        else:
            _source_dir = self._source_dir
        # If target is a directory
        if (self._target_dir.exists() and self._target_dir.is_dir()) or self._target_dir.suffix == "":
            return Path(self.source_path.as_posix().replace(_source_dir.as_posix(), self._target_dir.as_posix()))
        else:
            return self._target_dir

    def is_changed(self):
        src_stamp = os.stat(self.source_path).st_mtime
        try:
            trg_stamp = os.stat(self.target_path).st_mtime
            if src_stamp == trg_stamp:
                return False
            else:
                return True
        except FileNotFoundError:
            return True

    def __repr__(self):
        return f"PathElement: \nSource -> {self.source_path.as_posix()}\nTarget -> {self.target_path.as_posix()}"


class Archiver:

    # 65kB
    BUFFER_SIZE = 65536

    def __init__(self, source_path: str | Path, target_path: str | Path):
        self.source_path = Path(source_path)
        self.target_path = Path(target_path)
        self.files: set[PathElement] = set()
        self.check_objects_before_transfer()
        self.build_target_paths()

    def check_objects_before_transfer(self):
        if not self.source_path.exists():
            logger.error(f"Source path {self.source_path} does not exist!")
            exit(1)

        # src_path is a file and trg_path is a file
        # src_path is a file and trg_path is a directory
        # src_path is a directory and trg_path is a directory

        if self.source_path.is_dir() and (
                (self.target_path.exists() and self.target_path.is_file()) or self.target_path.suffix != ""):
            logger.error("Wrong parameter combination!")
            logger.error("There can only be:")
            logger.error("source - file,      target - file")
            logger.error("source - file,      target - directory")
            logger.error("source - directory, target - directory")
            exit(1)

    def build_target_paths(self):
        _files: set = set()
        if self.source_path.is_dir():
            _files.update(path for path in self.source_path.glob('**/*.*'))
        elif self.source_path.is_file():
            _files.add(self.source_path)
        else:
            logger.error("Unknown type. Not a directory nor a file.")
            exit(1)
        self.files = set(PathElement(_file, self.source_path, self.target_path) for _file in _files)

    @staticmethod
    def copy(file: PathElement):
        with open(file.source_path, 'rb') as src:
            with open(file.target_path, 'wb') as trg:
                while True:
                    data = src.read(Archiver.BUFFER_SIZE)
                    if not data:
                        break
                    trg.write(data)

    def transfer(self):
        logger.info("Transfering...")
        for file in (f for f in self.files if f.is_changed()):
            logger.debug(f"Creating directory: {file.target_path.parent}...")
            file.target_path.parent.mkdir(parents=True, exist_ok=True)
            # logger.debug(f"Copying file: {file.source_path} ---> {file.target_path}...")
            logger.debug(file)
            Archiver.copy(file)
            os.utime(file.target_path, (os.stat(file.target_path).st_mtime, os.stat(file.source_path).st_mtime))


def parse_args() -> argparse.Namespace:
    """
    This method parses input parameters

    :return: Namespace containing specified script arguments
    """
    parser = argparse.ArgumentParser(description='Parse input parameters')

    parser.add_argument('-s', '--source_path',
                        required=True,
                        help='Path to source file/directory to copy'
                        )
    parser.add_argument('-t', '--target_path',
                        help='Path to target file/directory to copy into'
                        )
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Debug mode'
                        )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    logger.info(f"Source path: {args.source_path}")
    logger.info(f"Target path: {args.target_path}")

    ts = time.perf_counter()
    archiver = Archiver(args.source_path, args.target_path)
    total = len(archiver.files)
    modified = len([f for f in archiver.files if f.is_changed()])
    unmodified = len([f for f in archiver.files if not f.is_changed()])
    logger.info(f"Files found: {total:>8}")
    logger.info(f"   Modified: {modified:>8}")
    logger.info(f" Unmodified: {unmodified:>8}")
    te = time.perf_counter()
    logger.info(f"Verification time: {te - ts:0.4f}s")

    if modified:
        ts = time.perf_counter()
        archiver.transfer()
        te = time.perf_counter()
        logger.info(f"Transfer time: {te - ts:0.4f}s")
    else:
        logger.info("Nothing to transfer")
